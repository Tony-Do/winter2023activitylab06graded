public class Board
{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
		
	public Board() 
	{
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];

	}
	
	public String toString()
	{
		String tilesString = "";
		for(int i = 0; i < this.tiles.length; i++)
		{
			if(!tiles[i])
			{
				tilesString += i+1;
			}
			else
			{
				tilesString += "X";
			}
			if(i < tiles.length-1)
			{
				tilesString += ",";
			}
		}

		return tilesString;
	}

	public void setTiles(boolean[] tiles)
	{
		this.tiles = tiles;

	}
	
	public boolean playATurn()
	{
		die1.roll();
		die2.roll();
		System.out.println("Die roll #1: " + die1 + ", " + "Die roll #2: " + die2);

		int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
		System.out.println("The sum is " + sumOfDice);

		if(!this.tiles[sumOfDice-1]) //if the sum is not closed //
		{
			this.tiles[sumOfDice-1] = true; // you close that tile //
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}

		else if(!this.tiles[this.die1.getFaceValue()-1]) // if the tile at die1 is not closed //
		{
				this.tiles[this.die1.getFaceValue()-1] = true; // close it if it is not //
				System.out.println("Closing tile with the same value as die one " + this.die1.getFaceValue());
				return false;
		} 

		else if(!this.tiles[this.die2.getFaceValue()-1]) // if second value is not closed //			
		{
				this.tiles[this.die2.getFaceValue()-1] = true;
				System.out.println("Closing tile with the same value as die two " + this.die2.getFaceValue());
				return false;
		}
		else
		{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
		
	}	
}