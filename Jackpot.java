public class Jackpot
{
	public static void main(String[]args)
	{
		System.out.println("Welcome to the board game!");

		Board board = new Board();
		
		boolean gameOver = false;
		
		int numOfTilesClosed = 0;
		
		while(!gameOver)
		{
			System.out.println(board);
			
			
			if(board.playATurn())
			{
				gameOver = true;
			}
			else
			{
				numOfTilesClosed++;
			}
		}
		System.out.println("This is the number of tiles closed " + numOfTilesClosed);
		if(numOfTilesClosed >= 7)
		{
			System.out.println("The player reached a jackpot and won ");
		}
		else
		{
			System.out.println("You lost ");
		}
	}
}