import java.util.Random;
public class Die
{
	private int faceValue;
	private Random random;
	
	public Die() // constructor //
	{		
		this.faceValue = 1;	
		random = new Random();
	}
	
	
	public int getFaceValue()
	{
		return this.faceValue = faceValue;
	}
	public int roll()
	{
		return this.faceValue = random.nextInt(6) + 1;
	}
	
	public String toString()
	{
		return ""+(this.faceValue);
		
	}

}